# tool-kube-bench

kube-bench is a tool that checks whether Kubernetes is deployed securely by running the checks documented in the CIS Kubernetes Benchmark. For more details, please refer to https://github.com/aquasecurity/kube-bench.

## 前置作業
此專案將以 Minikube 示範，您可以基於您的 K8s 環境指定對應的 CIS benchmark。
> 若您想要在您的環境安裝 Minikube，請參考[安裝手冊](https://minikube.sigs.k8s.io/docs/start/)

## 快速開始
1. 下載 [kube-bench](https://github.com/aquasecurity/kube-bench) 官方專案：
    ```sh
    $ git clone https://github.com/aquasecurity/kube-bench
    $ cd kube-bench
    ```
2. 部署 *kube-bench* Job：
    ```sh
    $ kubectl apply -f job.yaml
    job.batch/kube-bench created
    # 若您使用的為託管的 K8s，e.g., GKE，請改成部署 job-gke.yaml
    ```
3. 查看執行結果：
    ```sh
    $ kubectl get pods
    NAME                  READY   STATUS      RESTARTS   AGE
    kube-bench--1-v4nlq   0/1     Completed   0          47s

    $ kubectl logs kube-bench--1-v4nlq
    [INFO] 1 Master Node Security Configuration
    [INFO] 1.1 Master Node Configuration Files
    [PASS] 1.1.1 Ensure that the API server pod specification file permissions are set to 644 or more restrictive (Automated)
    [PASS] 1.1.2 Ensure that the API server pod specification file ownership is set to root:root (Automated)
    [PASS] 1.1.3 Ensure that the controller manager pod specification file permissions are set to 644 or more restrictive (Automated)
    ...
    ```
    > 在沒指定 benchmark 版本的情況下，kube-bench 會依照環境自動判定適合的版本。但若要知道使用的 cis-benchmark 版本，則需要透過 debug 模式 (-v 3) 獲取

## 進階使用
### 使用 debug 模式
1. 複製 `job.yaml` 的檔案內容：
    ```sh
    $ cp job.yaml job-debug.yaml
    ```
2. 修改 `job-debug.yaml`：
    ```yaml
    ---
    apiVersion: batch/v1
    kind: Job
    metadata:
      name: kube-bench-debug  # 另外命名
    spec:
      template:
        metadata:
          labels:
            app: kube-bench-debug  # 另外打上標籤
        spec:
          hostPID: true
          containers:
            - name: kube-bench
              image: aquasec/kube-bench:0.6.3
              command: ["kube-bench", "v", "3"]  # 修改指令
    ```
3. 部署 *kube-bench-debug* Job：
    ```sh
    $ kubectl apply -f job-debug.yaml
    job.batch/kube-bench-debug created
    ```
4. 查看執行結果：
    ```sh
    $ kubectl get pods
    NAME                        READY   STATUS      RESTARTS   AGE
    ...
    kube-bench-debug--1-txbvl   0/1     Completed   0          4s

    $ kubectl logs kube-bench-debug--1-txbvl
    ```
    從以下的 debug 日誌中可看到，kube-bench 所對應的為 `cis-1.6`：
    ```
    I1104 11:23:17.663640    8970 util.go:286] Kubernetes REST API Reported version: &{1 22  v1.22.2}
    I1104 11:23:17.663767    8970 common.go:282] mapToBenchmarkVersion for k8sVersion: "1.22" cisVersion: "" found: false
    I1104 11:23:17.663843    8970 common.go:286] mapToBenchmarkVersion for k8sVersion: "1.21" cisVersion: "" found: false
    I1104 11:23:17.663852    8970 common.go:286] mapToBenchmarkVersion for k8sVersion: "1.20" cisVersion: "" found: false
    I1104 11:23:17.663859    8970 common.go:286] mapToBenchmarkVersion for k8sVersion: "1.19" cisVersion: "cis-1.6" found: true
    I1104 11:23:17.663868    8970 common.go:348] Mapped Kubernetes version: 1.22 to Benchmark version: cis-1.6
    I1104 11:23:17.663972    8970 common.go:351] Kubernetes version: "1.22" to Benchmark version: "cis-1.6"
    I1104 11:23:17.663997    8970 root.go:76] Running checks for benchmark cis-1.6
    ```

### 指定 CIS benchmark 版本
若要自行指定 CIS benchmark 的版本也可以修改 `job.yaml` 的 command，透過 `--benchmark` 參數指定:
1. 複製 `job.yaml` 的檔案內容：
    ```sh
    $ cp job.yaml job-1-5.yaml
    ```
2. 修改 `job-1-6.yaml`：
    ```yaml
    ---
    apiVersion: batch/v1
    kind: Job
    metadata:
      name: kube-bench-1-6  # 另外命名
    spec:
      template:
        metadata:
          labels:
            app: kube-bench-1-6  # 另外打上標籤
        spec:
          hostPID: true
          containers:
            - name: kube-bench
              image: aquasec/kube-bench:0.6.3
              command: ["kube-bench", "--benchmark", "cis-1.6"]  # 指定 benchmark 版本為 v1.6
    ```
3. 部署 *kube-bench-1-6* Job：
    ```sh
    $ kubectl apply -f job-1-6.yaml
    job.batch/kube-bench-1-6 created
    ```
4. 查看執行結果：
    ```sh
    $ kubectl get pods
    NAME                        READY   STATUS      RESTARTS   AGE
    ...
    kube-bench-1-6--1-4f6pl     0/1     Completed   0          18s

    $ kubectl logs kube-bench-debug--1-txbvl
    ...
    ```
    若採用 debug 模式，則會看到 kube-bench 直接以 benchmark cis-1.6 作為基準：
    ```sh
    I1104 11:34:14.536943   12340 util.go:286] Kubernetes REST API Reported version: &{1 22  v1.22.2}
    I1104 11:34:14.537005   12340 common.go:351] Kubernetes version: "" to Benchmark version: "cis-1.6"
    I1104 11:34:14.537015   12340 root.go:76] Running checks for benchmark cis-1.6
    ```

### 客製化測試項目
若執行完 kube-bench 後，結果為 FAIL 的測試項誤因環境因素可被忽略，但又不想讓結果顯示 `FAIL`，則需透過客製化 cfg 來調整測試項目。
以 FAIL 測試項目 4.2.6 為例 (基於 `cis-1.6`)：
```sh
[FAIL] 4.2.6 Ensure that the --protect-kernel-defaults argument is set to true (Automated)
```
步驟如下：
1. 首先，複製執行的 benchmark 的檔案內容：
    ```sh
    $ cp -r cfg/cis-1.6 cfg/cis-1.6-customized

    $ cd cfg/cis-1.6-customized
    ```
2. 由於測試項目 4.2.6 屬於 *Section 4. Worker Nodes*，因此對應的 config 為 `node.yaml`。修改 `node.yaml`：
    ```yaml
    - id: 4.2.6
      text: "Ensure that the --protect-kernel-defaults argument is set to true (Automated)"
      type: skip  # 新增 type 為 skip
    ```
3. 建立 ConfigMap：
    ```sh
    $ kubectl create configmap kube-bench-customized-cis-1-6-node --from-file=node.yaml
    configmap/kube-bench-customized-cis-1-6-node created
    ```
4. 複製 `job-1-6.yaml` 的檔案內容：
    ```sh
    $ cd ../../ # 回到 kube-bench 根目錄
    $ cp job-1-6.yaml job-customized.yaml
    ```
    > job-1-6.yaml 請參考**指定 CIS benchmark 版本**章節
5. 修改 `job-customized.yaml`：
    ```yaml
    ---
    apiVersion: batch/v1
    kind: Job
    metadata:
      name: kube-bench-1-6-customized  # 另外命名
    spec:
      template:
        metadata:
          labels:
            app: kube-bench-1-6-customized  # 另外命名
        spec:
          hostPID: true
          containers:
            - name: kube-bench
              image: aquasec/kube-bench:0.6.3
              command: ["kube-bench", "-v" , "3",  "--benchmark", "cis-1.6"]
              volumeMounts:
                - ...
                - name: node-config  # 將客製化的 node.yaml 掛載至容器
                  mountPath: "/opt/kube-bench/cfg/cis-1.6/node.yaml"
                  subPath: node.yaml
                  readOnly: true
          volumes:
            - ...
            - name: node-config  # 建立 volume
              configMap:
                name: kube-bench-customized-cis-1-6-node
      ```
6. 部署 *kube-bench-1-6-customized* Job：
    ```sh
    $ kubectl apply -f job-customized.yaml
    job.batch/kube-bench-1-6-customized created
    ```
7. 查看執行結果：
    ```sh
    $ kubectl get pods
    NAME                                 READY   STATUS      RESTARTS   AGE
    kube-bench-1-6-customized--1-l7xxm   0/1     Completed   0          60s

    $ kubectl logs kube-bench-1-6-customized--1-l7xxm
    ...
    [INFO] 4.2.6 Ensure that the --protect-kernel-defaults argument is set to true (Automated)
    ...
    ```
    > 測試項目 4.2.6 由原先的 `[FAIL]` 變為 `[INFO]`